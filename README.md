# k8s-vagrantbox


You just need to perform below things before going to get started
1.	Install Virtualbox on your machine
2.	Install Vagrant on your machine
3.	Create a user on Oracle Container Registry
4.	Download or clone Oracle Github repository
Since the top 3 listed items above are easy to follow, I will go straight to the step of cloning the Oracle Github repository then proceed the setup.
Clone the Oracle Github repository
$ git clone https://github.com/oracle/vagrant-boxes
Once done, just go into the Kubernetes folder
$ cd vagrant-boxes/Kubernetes/
Create the Kubernetes master node
Type in the command vagrant up master to bring up the master VM
$ vagrant up master
[..]
master: Complete!
master: net.bridge.bridge-nf-call-ip6tables = 1
master: net.bridge.bridge-nf-call-iptables = 1
master: Your Kubernetes VM is ready to use!
==> master: Running provisioner: shell...
master: Running: inline script
==> master: Running provisioner: shell...
master: Running: inline script
ssh into the master VM.
$ vagrant ssh master
then run the script kubeadm-setup-master.sh to provision the master node. You will be asked to provide credential to access the Oracle Container Registry
[vagrant@master ~]$ su -
[root@master ~]# /vagrant/scripts/kubeadm-setup-master.sh
/vagrant/scripts/kubeadm-setup-master.sh: Login to container-registry.oracle.com
Username: ngphban@gmail.com
Password:
Login Succeeded
/vagrant/scripts/kubeadm-setup-master.sh: Setup Master node -- be patient!
/vagrant/scripts/kubeadm-setup-master.sh: Copying admin.conf for vagrant user
/vagrant/scripts/kubeadm-setup-master.sh: Copying admin.conf into host directory
/vagrant/scripts/kubeadm-setup-master.sh: Saving token for worker nodes
/vagrant/scripts/kubeadm-setup-master.sh: Master node ready, run
/vagrant/scripts/kubeadm-setup-worker.sh
on the worker nodes
Create the Kubernetes worker nodes
Let’s start creating the first worker node (worker1):
To bring up worker1 VM, type in the command vagrant up worker1
$ vagrant up worker1
[..]
worker1: Complete!
worker1: net.bridge.bridge-nf-call-ip6tables = 1
worker1: net.bridge.bridge-nf-call-iptables = 1
worker1: Your Kubernetes VM is ready to use!
ssh into the worker1 VM
$ vagrant ssh worker1
and run the setup script kubeadm-setup-worker.sh using root privileges to provision the worker1 node. Again, remember to enter your username/password on Oracle Container Registrywhen you’re asked.
[vagrant@worker1 ~]$ su -
[root@worker1 ~]# /vagrant/scripts/kubeadm-setup-worker.sh
/vagrant/scripts/kubeadm-setup-worker.sh: Login to container-registry.oracle.com
Username: <YOUR_REGISTERED_EMAIL>
Password: <YOUR_PASSWORD>
Login Succeeded
/vagrant/scripts/kubeadm-setup-worker.sh: Setup Worker node
[..]

This node has joined the cluster:
* Certificate signing request was sent to master and a response
was received.
* The Kubelet was informed of the new secure connection details.

Run 'kubectl get nodes' on the master to see this node join the cluster.
/vagrant/scripts/kubeadm-setup-worker.sh: Worker node ready
Repeat same steps to bring up and provision the second worker node that is worker2.
Setup kubectl
In order to easily mange our Kubernetes cluster on the created master VM, we should install kubectl binaries on our host.
Install kubectl binaries
Follow this instruction to install the tool.
Setup kubectl config
ssh into the master node, then get the config
[vagrant@master ~]$ cat ~/.kube/config
apiVersion: v1
clusters:
- cluster:
certificate-authority-data: LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSUN5RENDQWJDZ0F3SUJBZ0lCQURBTkJna3Foa2lHOXcwQkFRc0ZBREFWTVJNd0VRWURWUVFERXdwcmRXSmwKY201bGRHVnpNQjRYRFRFNE1Ea3lNakV3TXpJME9Wb1hEVEk0TURreE9URXdNekkwT1Zvd0ZURVRNQkVHQTFVRQpBeE1LYTNWaVpYSnVaWFJsY3pDQ0FTSXdEUVlKS29aSWh2Y05BUUVCQlFBRGdnRVBBRENDQVFvQ2dnRUJBTlh1ClUwbjdxbk1EUjFWRzFwRmRSMGl4NE9EcThTL1RXdy9Wd01qdk1hZGFMVk9JaDJiTFpGM1UwcjN2MHlzY2FlM3QKcTFyRXBSZGJ6dllVcXV4TGwvbXA4OTRCNjBOekZBYncxUkNNQ2U5akFKcmp1WCtmQlAwbWhNZGdUVk4vMWhYMApZako2ZlRhOFovbGxnd0ZBNUh3OG1NNEx1d1VaU3BVSEJnZ1o2Wndpbmtha1ovZWxDeHZwL1VVOEdyRUVsOGc0CkZ4dDFZRmN2YkdXMmQ2QloydGdXcXZRODZVNTNwQ3FCRi9RZWNydE9JdGwrZWRyeEJQWmpkSXhmRlErTS9VSEQKRmN3blNzYVhQSE1qTWZjTHVQQTlMQ2ltNnRjbm5Ua2o5dm4zeU4vYVlWemU2SHVDZS8rbzgvNWw0Q3JGS0E4ZQpwL1EvZnA4WTNDL2d5TlMzeHZNQ0F3RUFBYU1qTUNFd0RnWURWUjBQQVFIL0JBUURBZ0trTUE4R0ExVWRFd0VCCi93UUZNQU1CQWY4d0RRWUpLb1pJaHZjTkFRRUxCUUFEZ2dFQkFBSi9PRGd2c0FxTFlFazZYbUdxYURmK1FVZ1cKbGNCSkNUZ2RBRkdnbm1ocGJ2V2R5c0ZVUzU1VjZueTNkWGVqRlNWbnlIbzZ4WGkreVRLcVZuRkc1TTFhWVF0TQoveGFPSEw1cWpPQXYydGZQL1RJYnZ3Q0l1Qmd4aXM3WDhFQmZJb001WVdRS0xPUit6dStGV2NYNjdOUjYxdHloCjBZMjVuV0hycnRWYlliWWNhL0J0NXpzaVlhT0sweG5rVHExS2VLMnlHOHBBVU1uVEp6MlNpMlU1SE5BZzZZSUgKcFRzMlFIdjE5VnoyTTJrOFRGSUtKc0kyV2ZPWHNYMEw1YVdCbmU2TDNrakZzYlJ5bXpSNDNMYWltTk1od2cyZgpQdEVsendyWDc4UzQ0c0dMTkFWYWZMNEl4eFU1MEp1Yit1dDQwV05LaXlEcjhUNkhhK1JRTnQ0N1IwWT0KLS0tLS1FTkQgQ0VSVElGSUNBVEUtLS0tLQo=
server: https://192.168.99.100:6443
name: kubernetes
contexts:
- context:
cluster: kubernetes
user: kubernetes-admin
name: kubernetes-admin@kubernetes
current-context: kubernetes-admin@kubernetes
kind: Config
preferences: {}
users:
- name: kubernetes-admin
user:
client-certificate-data: LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSUM4akNDQWRxZ0F3SUJBZ0lJSVltSlphaVNXYkV3RFFZSktvWklodmNOQVFFTEJRQXdGVEVUTUJFR0ExVUUKQXhNS2EzVmlaWEp1WlhSbGN6QWVGdzB4T0RBNU1qSXhNRE15TkRsYUZ3MHhPVEE1TWpJeE1ETXlOVEphTURReApGekFWQmdOVkJBb1REbk41YzNSbGJUcHRZWE4wWlhKek1Sa3dGd1lEVlFRREV4QnJkV0psY201bGRHVnpMV0ZrCmJXbHVNSUlCSWpBTkJna3Foa2lHOXcwQkFRRUZBQU9DQVE4QU1JSUJDZ0tDQVFFQXBITitjZmhCTUlVclg0cXgKT1VEUFcrZHVJeUhNUktkWE04TElnR1lXS3ZCcDVoRFdXZUZGdktqV2d6K2taaGJJYzFWRHN5QUFxNlZXK2N1MApyYlJaaERwT0tVQi9KVGM5Ty9MNzNISmJLZnp2NUxGQlBaN1E5aFpNelZJQ0FsdklVMTA3eDNvTlhmVG8vWHZLCjVtcXR3ZkxWcW43dHlwYzlmTFYxRkNDNUxoOWFjWVV0OVNaczlKdmhIK2ZRQWk1OEducy80bXdKZGlpcVYwcGoKNjdLRHFjRGIwdFg1cC9rK2s1VmxPWHYvTjV2SHBETlYrb1FWa0t5TnhLQ1E5N2hFUGpVSTdyYmFZNU1RZU9sWQpPcWZBY1RzLzJWcXFWczZqdkRwQ3lQN0VoN28zenUyc1J2bW5Sd1NIekU3S0FnUm9peXZlaUJLQXJWTy9jd3pQCnJ5UFF0d0lEQVFBQm95Y3dKVEFPQmdOVkhROEJBZjhFQkFNQ0JhQXdFd1lEVlIwbEJBd3dDZ1lJS3dZQkJRVUgKQXdJd0RRWUpLb1pJaHZjTkFRRUxCUUFEZ2dFQkFEM0tDMUlvUVYxTjQ1QTVIaDJhMThJRVhRcHllSW5aLzVkcAo5NS9GeGJxOUdIcGNHdGZTbGNVTGFwbDE5aFR0clA3Z1lyZmhoREZGN0tVV1NPRkhQY0kxNVg0WWRmWGd3KzB2CmhRMDN3c0E2MWljWG8wMWVQQ0lMMHpuZWZrN0dVMnFTV2NqTmdyOS8vQTBhRWl4REpHR21aSXNKRlZQN0dGZm0KMGQzenZucXZBTkxvbnRRRERZUVBGcEk5c3VrUGdpOFFwSk5JcGZORXk5R29NK2g3V1FHbFZPRkdMeFRIaC8vQQpLYUEzN1NoYXBiWVpqd0xReTBZam5SR0Q5UDZFcTh2djgwNjRsYVN2ZkZrWHRIQnhLeVo5dy96amFvQ1YrZXd0CmFvYjRaRnpnN1FBNVF0a2J6aWtIeWZLUTgrT2wrdk15TFlrMU9oaFFaRTl2ZCtvMjRoYz0KLS0tLS1FTkQgQ0VSVElGSUNBVEUtLS0tLQo=
client-key-data: LS0tLS1CRUdJTiBSU0EgUFJJVkFURSBLRVktLS0tLQpNSUlFb3dJQkFBS0NBUUVBcEhOK2NmaEJNSVVyWDRxeE9VRFBXK2R1SXlITVJLZFhNOExJZ0dZV0t2QnA1aERXCldlRkZ2S2pXZ3ora1poYkljMVZEc3lBQXE2VlcrY3UwcmJSWmhEcE9LVUIvSlRjOU8vTDczSEpiS2Z6djVMRkIKUFo3UTloWk16VklDQWx2SVUxMDd4M29OWGZUby9Ydks1bXF0d2ZMVnFuN3R5cGM5ZkxWMUZDQzVMaDlhY1lVdAo5U1pzOUp2aEgrZlFBaTU4R25zLzRtd0pkaWlxVjBwajY3S0RxY0RiMHRYNXAvaytrNVZsT1h2L041dkhwRE5WCitvUVZrS3lOeEtDUTk3aEVQalVJN3JiYVk1TVFlT2xZT3FmQWNUcy8yVnFxVnM2anZEcEN5UDdFaDdvM3p1MnMKUnZtblJ3U0h6RTdLQWdSb2l5dmVpQktBclZPL2N3elByeVBRdHdJREFRQUJBb0lCQUcvSC9ZUDBqa012MGRxZwppSFIxUkQyaEl2RUljVE9kVjUzY1liTTlNUVpBNG9xNm1Ta0JBZ2tKN3IzS0RiSkMvRC9CRGtKOGwxZ1ZDL2pCCmFXL0ZMaSs5L2hEMlFUYUh4THl5SGxhcStLS2l4bUY1eHZIZXVRVDlCWGx1cStUMGhFZCtFVHMzRUV6MFEwYkIKMVpvbm16SkJJTkZDYzlvSVVZZ3FRNG5KOEhXVFRSMzVVWVgzZ1EvYTI2bkJ4eWJINzd5Sm5lVnh2cVhUdHZtVQphdDFtMnpIaUVPNEl5L2FyZEUvd0Rud3JlTEx0WmZ5TXBzZHBZcHg0TkJoNEFtVE9lbDFmVnp0enJEUW5sZCttCjNXVXhkQlNqdVExUWxLbW8za1dvMDlGUXFYdHhLTTM2ckIrZWt4blFDc3lEQ1JmZFM5ZVQrOUw1NVhrUlV6RTcKbCszY1d3RUNnWUVBMVZBS3VTbzkwRWpWZXQrQmZBVGlUSnFXYnhScTRNbmhDWFQvcWJSeVVXVFpyVmZKcVNGTgpuQytUS0VZQXJ3MGZ4a1VYUmNvMVFWUHZaQWpLdWZkekV6QnNoeW1qc3MyVzVwU0pDWXcvMVdJTDB5RGE4VzNJCnhTZHNnaEtCUkFoajk3QTBmcUFaWjFla092MU1xVHNtQjhtc3V4dk14WDN6S3REcmYxSldkWUVDZ1lFQXhWeEoKdUpSYXpLN1ZMVk9hVUVrTGZGd0lPWHlwVjZrb1FKemwxRkFVQ20yeW1aZnpOUUlMZDVISGh1ZVRIMldXekJJZwozWm1NQ1pYSHE2bmt0UnVidHFxWW9Kb3VoRTk5c0hIa0l5VEZ1MjRIMDEvZWV2emRCYU9KckpaQ2dGZDJYS0Q0CmNCYVdyUVIzUkNnVzU1N09UODgzRVBLSStobS9hV3VlSjdnTmtqY0NnWUFmV3BtSTZBV1g5T3kwRitBdmU0UkwKNERrV2NMalkrN09CTVZwVzluZ0xNTGl5T3dJZ1F3M21WRFBjS3BYMUpRZXptL28wdnVrZkJoaGM2dEhwamM2egpWbDNOYkRoc2tyMklmUEs3bllyUWNZM3RaWm5vL01zUzk1MkNiUGZsS1JxUWtLeldvKyszK2ZEZzN0U0MxM2ZhCmt6RTVlM2VZMjg5L1RxQWU0ZHBsZ1FLQmdBblIvc1VWZExNc20rVG1mUTR6cEVPcTNLbm5ueEdDbEVNcXkzRFcKRHprNW5pSndZaVNvRm1QN3QvN3BkUE9oL1cvb0V3U1R5RmtOditLMTBIQ0drQ2Yxc3lTQ3AzL2o2TEFuNnA4cApERUc0UnVGb3FzeFoyckdXSXBxWTIzUkJaZDFVRVNpYXB5S3l6ekJ3QWp2Q05pZlllcmY0S1lLd1YwcnFsUE1MCkFIbmZBb0dCQUoxL3pOdW9nVFNhT2o1d242Y0JzYkNaZmpqQ0w0SHg3SlZnTWMvQlJkcmRoR21CbGc2Yk9YWWcKNFpTakpkYmsxaG51aTJENHhNQWh4cU1TWTlRQXhmQ3VvZzBIRjA4Tlg3bmkvaGMyWHB1WStUdUgzbFcvc3NHcwpoVjZrS1M5ZHBvUGNYL1QzeFlZZlZ3M2tBVU11bldiRUdpQXZhKzByc0FYZU5EUXN5QktJCi0tLS0tRU5EIFJTQSBQUklWQVRFIEtFWS0tLS0tCg==

Paste the above config onto the according location on your host machine. For example, this is my kubectl config file:
$ ll ~annpb/.kube/config
-rw------- 1 annpb annpb 5449 Sep 22 18:04 /home/annpb/.kube/config
Your Kubernetes cluster is now ready!

